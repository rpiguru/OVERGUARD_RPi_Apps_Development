import os
import threading
from functools import partial
import gc
import time
from kivy import Logger
from kivy.app import App
from kivy.clock import mainthread, Clock

import kivy_config

from kivy.base import ExceptionManager, ExceptionHandler
from kivy.uix.screenmanager import NoTransition, ScreenManager, SlideTransition
import widgets.widget_manager
from db import Bracelet
from kivy.config import _is_rpi
from kivymd.theming import ThemeManager
from screens.connect_bracelet import ConnectBraceletScreen
from screens.list import ListScreen
from screens.menu import MenuScreen
from screens.settings import SettingsScreen
from screens.setup_boat import SetupBoatScreen
from screens.start import StartScreen
from screens.terms_and_condition import TCScreen
from utils.config_util import get_config, set_config
from utils.uart_manager import UARTManager
import Queue

if _is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    output_pin = int(get_config('general', 'output_pin', 22))
    GPIO.setup(output_pin, GPIO.OUT)


class OverGuardExceptionHandler(ExceptionHandler):
    def handle_exception(self, exception):
        Logger.exception('Exception: {}'.format(repr(exception)))
        if isinstance(exception, KeyboardInterrupt):
            return ExceptionManager.RAISE
        return ExceptionManager.PASS


ExceptionManager.add_handler(OverGuardExceptionHandler())


q = Queue.Queue()


class OverGuardApp(App):

    screens = {}
    sm = None
    theme_cls = ThemeManager(theme_style='Dark')
    cur_screen = None

    def build(self):
        self.screens = {
            'start': StartScreen,
            'terms_and_condition': TCScreen,
            'setup_boat': SetupBoatScreen,
            'connect_bracelet': ConnectBraceletScreen,
            'menu': MenuScreen,
            'list': ListScreen,
            'settings': SettingsScreen,
        }

        self.sm = ScreenManager()

        if not Bracelet.table_exists():
            Bracelet.create_table()

        threading.Thread(target=self.start_uart).start()

        # Switch to the default screen.
        self.go_screen('start')
        # self.go_screen('list')
        return self.sm

    def go_screen(self, destination, direction=None):
        """
        Switch the current screen of screen manager
        :param direction:
        :param destination:
        :return:
        """
        try:
            if direction is None:
                self.sm.transition = NoTransition()
            else:
                self.sm.transition = SlideTransition(direction=direction)
            screen = self.screens[destination](name=destination)
            self.sm.switch_to(screen=screen)
            if self.cur_screen:
                self.sm.remove_widget(self.cur_screen)
                del self.cur_screen
                gc.collect()
            self.cur_screen = screen
            print 'Switched to {} screen'.format(destination)
            if _is_rpi:
                if destination == 'list':
                    GPIO.output(output_pin, GPIO.HIGH)
                elif destination == 'menu':
                    GPIO.output(output_pin, GPIO.LOW)

        except Exception as e:
            Logger.error('OverGuard: Failed to move to {} - {}'.format(destination, e))

    @staticmethod
    def send_data(msg_type='', msg=None):
        q.put({'type': msg_type, 'msg': msg})

    @mainthread
    def on_received_data(self, d, *args):
        if d['type'] == 'CMD':
            if d['message'] == {'UART': 'TEST'}:
                self.send_data(msg_type='RSP', msg={'UART': 'OK'})
            elif d['message'] == {'SYSTEM': 'REBOOT'}:
                print('Rebooting myself...')
                self.send_data(msg_type='RSP', msg={'SYSTEM': 'REBOOT_OK'})
                os.system('sudo reboot')
            elif d['message'] == {'SYSTEM': 'SHUTDOWN'}:
                print('Shutdown myself...')
                os.system('sudo shutdown -h now')
            elif d['message'] == {'GET': 'SCREEN'}:
                self.send_data(msg_type='RSP', msg={'SCREEN': str(self.cur_screen.number)})
            elif 'KEEP_ALIVE_TIMER' in d['message'].keys():
                new_time = int(d['message']['KEEP_ALIVE_TIMER'])
                print 'New KEEP_ALIVE_TIMER received - {}'.format(new_time)
                set_config('general', 'braceletkeepalivetimer', new_time)
                self.send_data(msg_type='RSP', msg={'KEEP_ALIVE_TIMER': str(new_time)})
            elif 'BRACELET_ID' in d['message'].keys():
                bracelet_id = d['message']['BRACELET_ID']
                if self.cur_screen.name == 'connect_bracelet':
                    print('New Bracelet ID received: {}'.format(bracelet_id))
                    Clock.schedule_once(partial(self.cur_screen.on_new_bracelet, bracelet_id))
            else:
                print('Invalid CMD message: {}'.format(d))
        elif d['type'] == 'EVT' and self.cur_screen.name == 'list':
            print('Event message received: {}'.format(d))
            Clock.schedule_once(partial(self.cur_screen.on_received_message, d['message']))
        else:
            print('Wrong message received: {}'.format(d))

    def start_uart(self, *args):
        port = get_config(section='uart', option='port', default='/dev/ttyS0')
        baudrate = int(get_config(section='uart', option='baudrate', default=115200))
        timeout = float(get_config(section='uart', option='timeout', default=2))
        uart = UARTManager(port=port, baudrate=baudrate, timeout=timeout)
        uart.send_msg(msg_type='RSP', msg={'BOOT': 'OK'})
        while True:
            data = uart.read_data()
            if data:
                Clock.schedule_once(partial(self.on_received_data, data))
            if not q.empty():
                d = q.get()
                uart.send_msg(msg_type=d['type'], msg=d['msg'])
            time.sleep(.01)


if __name__ == '__main__':
    app = OverGuardApp()
    try:
        app.run()
    finally:
        pass
