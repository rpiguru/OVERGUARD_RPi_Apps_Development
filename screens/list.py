import os
import random
from functools import partial

from kivy.clock import Clock
from kivy.lang import Builder
from peewee import *
from db import Bracelet
from kivymd.snackbar import Snackbar
from screens.base import BaseScreen
from widgets.bracelet_item import BraceletItem
from widgets.button import OGButton
from widgets.dialog import OverDlg, BraceletDetailDialog
from kivy.config import _is_rpi

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'list.kv'))


class ListScreen(BaseScreen):

    number = 7
    item_list = []
    is_popup_open = False

    def on_enter(self, *args):
        super(ListScreen, self).on_enter(*args)
        self.item_list = []
        Clock.schedule_once(self.get_registered_bracelets)
        if not _is_rpi:
            btn = OGButton(size_hint=(.17, .1), text='TEST', button_type='gray',
                           pos_hint={'center_x': .75, 'center_y': .1})
            btn.bind(on_release=self.on_btn_test)
            self.add_widget(btn)

    def get_registered_bracelets(self, *args):
        self.ids.box.height = 0
        for b in Bracelet.select():
            item = BraceletItem(b_id=b.bracelet_id, name=b.name, perimeter=b.perimeter)
            item.state = 'normal' if b.perimeter else 'dead'
            self.item_list.append(item)
            self.ids.box.add_widget(item)
            item.bind(on_pressed=self.on_item_pressed)
            self.ids.box.height += 33

    def on_btn_back(self):
        self.switch_screen('menu', 'up')

    def on_received_message(self, msg, *args):
        id_list = [b.b_id for b in self.item_list]
        if msg['BRACELET_ID'] not in id_list:
            print('Received {}, but this is not registered to this device'.format(msg))
            return
        item = self.item_list[id_list.index(msg['BRACELET_ID'])]
        if not item.perimeter:
            print 'Event received, but PERIMETER is OFF...'
            return
        if msg['STATUS'] == 'ON-BOARD':
            item.rssi = msg['RSSI']
            item.status = msg['STATUS']
            item.reset_timer()
        elif msg['STATUS'] == 'OVER-BOARD':
            if self.is_popup_open:
                print 'OVER-BOARD received, but a popup is already opened now. ignoring...'
                return
            if item.state != 'over':
                self.open_over_popup()
            item.state = 'over'
            item.rssi = msg.get('RSSI', '')
            item.status = msg['STATUS']
            item.update_status()

    def open_over_popup(self):
        dlg = OverDlg()
        dlg.bind(on_cancel=self.send_cancel)
        dlg.bind(on_dismiss=partial(self._update_popup_state, False))
        self._update_popup_state(True)
        dlg.open()

    def send_cancel(self, *args):
        self.app.send_data(msg_type='RSP', msg={'ALERT': 'CANCEL'})

    def on_item_pressed(self, *args):
        item = args[0]
        dlg = BraceletDetailDialog(bracelet_id=item.b_id, name=item.name, perimeter=item.perimeter)
        dlg.bind(on_save=self.on_update_bracelet)
        dlg.bind(on_blink=self.on_blink)
        dlg.bind(on_delete=self.on_delete_item)
        dlg.bind(on_dismiss=partial(self._update_popup_state, False))
        self._update_popup_state(True)
        dlg.open()

    def on_update_bracelet(self, *args):
        b_info = args[1]
        item = Bracelet.get(Bracelet.bracelet_id == b_info['bracelet_id'])
        item.name = b_info['name']
        item.perimeter = b_info['perimeter']
        item.save()
        for wid in self.ids.box.children:
            if wid.b_id == b_info['bracelet_id']:
                wid.update_perimeter(b_info['perimeter'])
                wid.name = b_info['name']
        Snackbar(text='Bracelet item is updated').show()

    def on_delete_item(self, *args):
        bracelet_id = args[0].bracelet_id
        item = Bracelet.get(Bracelet.bracelet_id == bracelet_id)
        item.delete_instance()
        name = item.name
        for wid in self.ids.box.children:
            if wid.b_id == bracelet_id:
                self.ids.box.remove_widget(wid)
                self.ids.box.height -= 33
        Snackbar(text='Deleted {}'.format(name)).show()

    def on_btn_test(self, *args):
        query = Bracelet.select().order_by(fn.Random())
        b = query.get()
        status = random.choice(['OVER-BOARD', 'ON-BOARD'])
        b_info = {'BRACELET_ID': b.bracelet_id, 'STATUS': status, 'RSSI': '-85dB'}
        self.on_received_message(b_info)

    def _update_popup_state(self, val, *args):
        self.is_popup_open = val
