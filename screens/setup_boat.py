import os

from kivy.clock import Clock
from kivy.lang import Builder
from screens.base import BaseScreen
from utils.config_util import get_config, set_config
from widgets.dialog import ErrorDialog
from widgets.draggable_box import DragBox

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'setup_boat.kv'))


class SetupBoatScreen(BaseScreen):

    boat_size = 'medium'
    drag_image = None
    number = 4

    def on_enter(self, *args):
        super(SetupBoatScreen, self).on_enter(*args)
        pos_x = int(get_config('boat', 'pos_x', 350))
        pos_y = int(get_config('boat', 'pos_y', 230))
        self.drag_image = DragBox(size_hint=(None, None), size=(100, 100), pos=(pos_x, pos_y),
                                  source='assets/circle.png', bound_zone_objects=[self.ids.box, ],
                                  droppable_zone_objects=[self.ids.box, ])
        self.add_widget(self.drag_image)
        self.ids.boat_name.text = get_config('boat', 'name', 'Input Name')
        size = get_config('boat', 'size', 'medium')
        self.on_btn_size(self.ids[size], size)

    def on_btn_back(self):
        self.switch_screen('terms_and_condition', 'right')

    def on_btn_save(self):
        if self.ids.boat_name.text in ['', 'Input Name']:
            ErrorDialog(message='Please input Boat Name').open()
        else:
            Clock.schedule_once(lambda dt: self.save_boat_info())
            self.switch_screen('connect_bracelet', 'left')

    def save_boat_info(self):
        name = self.ids.boat_name.text
        set_config('boat', 'pos_x', str(int(self.drag_image.pos[0])))
        set_config('boat', 'pos_y', str(int(self.drag_image.pos[1])))
        set_config('boat', 'name', name)
        set_config('boat', 'size', self.boat_size)
        self.app.send_data(msg_type='RSP', msg={'BOAT_SIZE': self.boat_size.upper(), 'BOAT_NAME': name})

    def on_btn_size(self, btn, _type):
        btn.button_type = 'blue'
        btn.color = [1, 1, 1, 1]
        buttons = ['small', 'medium', 'large']
        buttons.remove(_type)
        for _id in buttons:
            self.ids[_id].button_type = 'gray'
            self.ids[_id].color = [.6, .6, .6, 1]
        self.boat_size = _type

    def on_pre_leave(self, *args):
        self.drag_image.deparent()
        super(SetupBoatScreen, self).on_pre_leave(*args)
