from kivy.app import App
from kivy.properties import NumericProperty
from kivy.uix.screenmanager import Screen


class BaseScreen(Screen):

    app = None
    number = NumericProperty()

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()

    def switch_screen(self, screen_name, direction=None):
        self.app.go_screen(screen_name, direction)

    def on_blink(self, *args):
        self.app.send_data(msg_type='RSP', msg={'STROBE_LIGHT': 'BLINK'})
