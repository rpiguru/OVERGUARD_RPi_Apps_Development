import os

from kivy.clock import Clock
from kivy.lang import Builder
from screens.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'terms_and_condition.kv'))


class TCScreen(BaseScreen):

    number = 3

    def __init__(self, **kwargs):
        super(TCScreen, self).__init__(**kwargs)
        Clock.schedule_once(self._update_content)

    def _update_content(self, *args):
        with open('assets/tc.txt', 'rb') as fp:
            content = fp.read().decode('UTF-8')
        self.ids.content.text = content

    def on_btn_decline(self):
        self.switch_screen('start', 'right')

    def on_btn_accept(self):
        self.switch_screen('setup_boat', 'left')
