import os
from kivy.lang import Builder
from screens.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'start.kv'))


class StartScreen(BaseScreen):

    number = 2

    def on_btn_start(self):
        self.switch_screen('terms_and_condition', 'left')
