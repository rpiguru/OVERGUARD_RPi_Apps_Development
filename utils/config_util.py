import sys

if not sys.version_info > (3, 0):
    import ConfigParser as configparser
else:
    import configparser

import os

config = configparser.ConfigParser()

config_file = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), os.pardir, 'config.ini')


def get_config(section, option, default=None):
    try:
        config.read(config_file)
        return config.get(section=section, option=option)
    except configparser.NoSectionError:
        return default
    except configparser.NoOptionError:
        return default


def set_config(section, option, value):
    try:
        config.read(config_file)
    except Exception as e:
        print('Failed to read config file: {}'.format(e))
        return False

    if section not in config.sections():
        config.add_section(section=section)

    config.set(section=section, option=option, value=value)
    try:
        with open(config_file, 'w') as configfile:
            config.write(configfile)
        return True
    except Exception as e:
        print('Failed to write new config to file: {}'.format(e))
        return False
