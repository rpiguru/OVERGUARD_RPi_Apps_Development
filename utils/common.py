import os


def is_rpi():
    return 'arm' in os.uname()[4]
