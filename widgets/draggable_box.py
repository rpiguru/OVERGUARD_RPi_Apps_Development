from kivy.factory import Factory
from kivy.uix.image import Image
from widgets.DragNDropWidget import DragNDropWidget


class DragBox(DragNDropWidget, Image):
    pass


Factory.register('DragImage', cls=DragBox)
