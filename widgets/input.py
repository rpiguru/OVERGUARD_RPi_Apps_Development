from kivy.factory import Factory
from kivy.properties import BooleanProperty, StringProperty
from kivy.clock import Clock
from kivymd.textfields import MDTextField
from kivy.core.window import Window
from widgets.dialog import InputDialog


class OGMDTextField(MDTextField):
    use_dlg = BooleanProperty(False)
    halign = StringProperty('left')
    font_name = StringProperty('EUROSTILE')

    def __init__(self, **kwargs):
        super(OGMDTextField, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_dlg)

    def bind_dlg(self, *args):
        if self.use_dlg:
            self.bind(focus=self.show_dlg)

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            val = self.text if self.text != 'Input Name' else ''
            dlg = InputDialog(text=val, hint_text=self.hint_text, input_filter=self.input_filter)
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()

    def on_confirm(self, *args):
        self.text = args[1]
        self.dispatch('on_text_validate')

    def on_text(self, *args):
        super(OGMDTextField, self).on_text(*args)
        if self.halign == 'center':
            text_width = self._get_text_width(self.text, self.tab_width, self._label_cached)
            self.padding_x = (self.width - text_width) / 2


Factory.register('OGMDTextField', cls=OGMDTextField)
