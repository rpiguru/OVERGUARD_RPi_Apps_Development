import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from utils.config_util import get_config

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'bracelet_item.kv'))


class BraceletItem(BoxLayout):

    b_id = StringProperty()
    name = StringProperty()
    rssi = StringProperty()
    status = StringProperty()
    perimeter = BooleanProperty()
    state = StringProperty('idle')
    fore_color = ListProperty([1, 1, 1, 1])
    bg_color = ListProperty([0, 0, .3, .3])
    circle_color = ListProperty([.3, .3, .3, .8])

    timer = None

    def __init__(self, **kwargs):
        super(BraceletItem, self).__init__(**kwargs)
        self.register_event_type('on_pressed')
        Clock.schedule_once(self.on_state)
        Clock.schedule_once(self.reset_timer)

    def reset_timer(self, *args):
        if self.timer:
            self.timer.cancel()
            self.timer = None
        if self.state != 'dead':
            timeout = int(get_config('general', 'braceletkeepalivetimer', 10))
            self.timer = Clock.schedule_once(self.set_idle, timeout)
            self.state = 'normal'
        self.update_status()

    def update_status(self):
        self.ids.rssi.text = self.rssi
        self.ids.status.text = self.status

    def set_idle(self, *args):
        if self.state != 'over':
            self.state = 'idle'

    def on_state(self, *args):
        if self.state == 'over':
            self.fore_color = [1, .06, .06, 1]
        elif self.state == 'normal':
            self.fore_color = [1, 1, 1, .8]
        else:
            self.fore_color = [.8, .8, .8, .8]

        if self.state == 'normal':
            self.bg_color = [.007, .486, .780, .7]
        elif self.state == 'idle':
            self.bg_color = [.007, .231, .368, .4]
        elif self.state == 'dead':
            self.bg_color = [.25, .25, .25, .3]
        elif self.state == 'over':
            self.bg_color = [.3, 0, 0, .3]
        self.ids.img_status.source = 'assets/status_{}.png'.format(self.state)
        if self.perimeter:
            self.ids.switch_perimeter.source = 'assets/perimeter_on.png'
        else:
            self.ids.switch_perimeter.source = 'assets/perimeter_off.png'

    def update_perimeter(self, val):
        self.perimeter = val
        if self.perimeter:
            self.ids.switch_perimeter.source = 'assets/perimeter_on.png'
        else:
            self.ids.switch_perimeter.source = 'assets/perimeter_off.png'
        if self.state != 'over':
            self.state = 'normal' if self.perimeter else 'dead'

    def on_touch_down(self, touch):
        super(BraceletItem, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            self.dispatch('on_pressed')

    def on_pressed(self):
        pass
